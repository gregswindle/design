# design 🎱

> When the objects we use every day and the surroundings we live in have become in themselves a work of art, then we shall be able to say that we have achieved a balanced life.
>
> — Murani, Bruno. Design as Art.

![gregswindle/design logo][logo]

## menu

[![milestone][the-john-ardor-house-badge]][the-john-ardor-house-page]

## about

This repository reflects the design lessons I [learn in public ![Go to the Learning in Public Site][octicon-link-external]][learning-in-public].

### motivations

- ❽ behold beauty born of purpose
- ❽ seek sensual insights from hidden dimensions

    > ![citation][octicon-quote] <cite>A fictional social network diagram. It consists of 165 Nodes and 1851 Edges.
    > The SVG-file was auto-generated by script. The underlying node/edge data can be extracted from the circle/line elements.</cite> [^1]
    >
    > ![Social Network Diagram (large)][social-network-diagram-large]

## color

[`#0F4C81` ![Learning about Pantone's 2020 color of the year][octicon-link-external]][2020-shop-pantone]

## copyright

[CC0 1.0][cc0-1.0-license] © 2019 [greg**swindle**][contact] 🎱

_logo by [icons8][icons8-url]_

## references

[^1]: DarwinPeacock, M. (2014) _File:Social Network Diagram (large).svg - Wikimedia Commons_. Retrieved March 24, 2019, from <https://commons.wikimedia.org/wiki/File:Social_Network_Diagram_(large).svg>

<!-- ⛔️ Do not remove this line or anything under it. ⛔️ -->

[2020-shop-pantone]: https://www.pantone.com/color-intelligence/color-of-the-year/color-of-the-year-2020 "Color: \"Classic Blue\", Pantone's 2020 color of the year."

[cc0-1.0-license]: LICENSE

[contact]: https://gitlab.com/gregswindle

[icons8-url]: https://icons8.com/

[learning-in-public]: https://www.verbaltovisual.com/learn-in-public/

[logo]: https://assets.gitlab-static.net/uploads/-/system/project/avatar/11422742/gregswindle-design-logo.png?width=240

[octicon-link-external]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.5.0/svg/link-external.svg

[octicon-quote]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.5.0/svg/quote.svg

[social-network-diagram-large]: assets/social-network-diagram-large.png

[the-john-ardor-house-badge]: https://img.shields.io/badge/view-the%20john%20ardor%20house%20➡-0F4C81.svg?style=for-the-badge&logoColor=999

[the-john-ardor-house-page]: https://gitlab.com/gregswindle/design/-/wikis/home#the-john-ardor-house
